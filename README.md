# Phrase Search

Search for text in a set of documents. The output will include the next most
frequently used word in the phrase.

## Getting Started

The tool is a command line tool, that must be executed in a console.

Typing `/help` or `/?` in the tool will print a short description of different commands. The
output will look like
```
Enter phrase to search or type /exit to exit or /? for more help.

> /help
Usage:
  <phrase>                          Phrase to search.
  /index                            Show currently indexed files.
  /index [ <file> | directory> ]    Index the file (if the argument is a file)
                                    or the files in the directory (excluding sub-directories).
  /exit                             Exit the tool.
  /help | /?                        Get information about using the tool.

```

### Building

To build the project use
```
mvn clean install
```
The build is using **Apache Maven Assembly Plugin** to produce an executable JAR. 

### Running

Because the tool is using `java.io.Console`, that is obtained from `System.console()`,
depending on the used IDE, it might not be possible to run the tool directly from the IDE.

To run the tool use
```
mvn exec:java
```
or
```
java -jar phrase-search-1.0-SNAPSHOT-jar-with-dependencies.jar
```

## Operation

![Data Structure](docs/data-structure.png)

The index holds a collection of indexed documents.

Each document has two major components - **content** and **index**. The content is normalized,
i.e. it stores the tokens without any separators. The index is a tree, where each token stores
a list of offsets in the content.

When a search operation is performed, the following steps are executed:

1. The list of offsets for the first token in the search phrase is obtained from the index.
2. For each offset a new search result is created. All search results are stored in a list.
3. Then, for each search result, the next token is added to it (the next token is taken
from the content).

Then, for each remaining token in the search phrase, the following steps are executed:

1. The list with search results is filtered - the search results, whose last token is different
than the current token from the search phrase, are moved from the list.
2. Then, for each search result, the next token is added to it (the next token is taken
from the content).

## Dependencies

The tool is using the following 3rd party libraries:

* [Apache Commons Collections](https://commons.apache.org/proper/commons-collections/)
