/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;

import java.util.stream.Stream;

import com.acme.phrasesearch.Utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndexBuilderTest {
    private Index index;

    private IndexBuilder indexBuilder;

    public IndexBuilderTest() {
        // No-op
    }

    @Before
    public void setUp() {
        index = new Index();
        indexBuilder = new IndexBuilder(index, this::tokenSupplier, this::write);
    }

    @Test
    public void testRun() {
        //
        // Given
        Map<Document, Boolean> sourceDocuments = new HashMap<>();

        sourceDocuments.put(Document.of(Paths.get("-input-1.txt")), false);
        sourceDocuments.put(Document.of(Paths.get("-input-2.txt")), false);

        Map<Document, Boolean> failDocuments = new HashMap<>();

        failDocuments.put(Document.of(Paths.get("-fail-1.txt")), false);
        failDocuments.put(Document.of(Paths.get("-fail-2.txt")), false);

        Map<Document, Boolean> allDocs = new HashMap<>();

        allDocs.putAll(sourceDocuments);
        allDocs.putAll(failDocuments);

        //
        // When
        indexBuilder.run(Stream.of(allDocs.keySet().stream()
                                          .map(Document::getName)
                                          .toArray(Path[]::new)));

        //
        // Then
        Set<Document> documents = index.getDocuments();

        assertEquals("documents.size", sourceDocuments.size(), documents.size());

        for (Document c : documents) {
            for (Map.Entry<Document, Boolean> e : sourceDocuments.entrySet()) {
                if (c.getName().equals(e.getKey().getName())) {
                    e.setValue(true);
                }
            }
        }

        for (Map.Entry<Document, Boolean> e : sourceDocuments.entrySet()) {
            assertTrue(e.getKey().toString(), e.getValue());
        }
    }

    private Stream<String> tokenSupplier(final Document document) {
        if (document.getName().toString().startsWith("-fail")) {
            throw new RuntimeException("The document [" + document.getName() + "] must fail");
        }

        return new SimpleTokenSupplier().stream(Utils.resource(IndexBuilderTest.class, document.getName().toString()));
    }

    private void write(final String text) {
        // /dev/null
    }
}
