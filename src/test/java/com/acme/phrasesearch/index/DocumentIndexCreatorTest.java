/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.nio.file.Paths;

import java.util.stream.Stream;

import com.acme.phrasesearch.utils.StringDedup;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DocumentIndexCreatorTest {
    private DocumentIndexCreator documentIndexCreator;

    public DocumentIndexCreatorTest() {
        // No-op
    }

    @Before
    public void setUp() {
        documentIndexCreator = new DocumentIndexCreator(Document.of(Paths.get("/document")));
    }

    @Test
    public void testIndex() {
        StringDedup stringDedup = new StringDedup();

        //
        // When
        documentIndexCreator.index(this::tokenSupplier, stringDedup);

        //
        // Then
        assertEquals("distinctTokens", 5, documentIndexCreator.getDocument().getDistinctTokensCount());
        assertEquals("allTokens", 11, documentIndexCreator.getDocument().getAllTokensCount());

        verify("a", 0, 6, 10, 14, 20);
        verify("b", 2, 16);
        verify("c", 8);
        verify("d", 12);
        verify("r", 4, 18);
    }

    private void verify(String token, int... expected) {
        Token[] offsets = documentIndexCreator.getDocument().findTokens(token);

        int[] array = new int[offsets.length];

        for (int i = 0; i < array.length; i++) {
            array[i] = offsets[i].offset();
        }

        assertArrayEquals(token, expected, array);
    }

    private Stream<String> tokenSupplier(final Document document) {
        return Stream.of("a", "b", "r", "a", "c", "a", "d", "a", "b", "r", "a");
    }
}
