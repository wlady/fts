/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.util.List;

import static java.util.stream.Collectors.toList;

import com.acme.phrasesearch.Utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleTokenSupplierTest {
    private static final String INPUT = "-input.txt";
    private static final String OUTPUT = "-output.txt";

    private SimpleTokenSupplier tokenSupplier;

    public SimpleTokenSupplierTest() {
        // No-op
    }

    @Before
    public void setUp() {
        tokenSupplier = new SimpleTokenSupplier();
    }

    @Test
    public void testStream() {
        //
        // When
        List<String> tokens = tokenSupplier.stream(Utils.resource(SimpleTokenSupplierTest.class, INPUT))
                                           .collect(toList());

        //
        // Then
        List<String> expected = Utils.resource(SimpleTokenSupplierTest.class, OUTPUT)
                                     .filter(c -> !c.startsWith("-"))
                                     .collect(toList());

        assertEquals("tokens", expected, tokens);
    }
}
