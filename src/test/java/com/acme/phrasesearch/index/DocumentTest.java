/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.nio.file.Paths;

import java.util.stream.Stream;

import com.acme.phrasesearch.utils.StringDedup;

import com.acme.phrasesearch.Utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DocumentTest {
    private Document document;

    public DocumentTest() {
        // No-op
    }

    @Before
    public void setUp() {
        document = Document.of(Paths.get("/document"));

        prepareDocument();
    }

    @Test
    public void testFindTokens_for_single_match() {
        //
        // When
        Token[] tokens = document.findTokens("imperdiet");

        //
        // Then
        assertEquals("imperdiet.count", 1, tokens.length);
        assertEquals("imperdiet[0]", 242, tokens[0].offset());

        //
        // When
        Token nextToken = document.getNextToken(tokens[0].offset());

        //
        // Then
        assertEquals("imperdiet[0]:next(pulvinar).offset", 252, nextToken.offset());
        assertEquals("imperdiet[0]:next(pulvinar).token", "pulvinar", nextToken.token());
    }

    @Test
    public void testFindTokens_for_multi_match() {
        //
        // When
        Token[] tokens = document.findTokens("euismod");

        //
        // Then
        assertEquals("euismod.count", 2, tokens.length);
        assertEquals("euismod[0].offset", 79, tokens[0].offset());
        assertEquals("euismod[1].offset", 297, tokens[1].offset());

        //
        // When
        Token nextToken = document.getNextToken(tokens[0].offset());

        //
        // Then
        assertEquals("euismod[0]:next(dignissim).offset", 87, nextToken.offset());
        assertEquals("euismod[0]:next(dignissim).token", "dignissim", nextToken.token());

        //
        // When
        nextToken = document.getNextToken(tokens[1].offset());

        //
        // Then
        assertEquals("euismod[1]:next(molestie).offset", 305, nextToken.offset());
        assertEquals("euismod[1]:next(molestie).token", "molestie", nextToken.token());
    }

    @Test
    public void testFindTokens_should_return_empty_array_no_match() {
        //
        // When
        Token[] tokens = document.findTokens("NoMatch");

        //
        // Then
        assertEquals("tokens.count", 0, tokens.length);
    }

    @Test
    public void testGetNextToken_should_return_null_for_last_token() {
        //
        // When
        Token[] tokens = document.findTokens("malesuada");

        //
        // Then
        assertEquals("malesuada.count", 1, tokens.length);
        assertEquals("malesuada[0].offset", 476, tokens[0].offset());

        //
        // When
        Token nextToken = document.getNextToken(tokens[0].offset());

        //
        // Then
        assertNull("malesuada[0]:next(null)", nextToken);
    }

    @Test
    public void testEquals() {
        //
        // Given
        Document doc1_1 = new Document(1, Paths.get("/doc-1"));
        Document doc1_2 = new Document(1, Paths.get("/doc-2"));
        Document doc2_1 = new Document(2, Paths.get("/doc-1"));
        Document doc2_2 = new Document(2, Paths.get("/doc-2"));

        //
        // When
        boolean equals = doc1_1.equals(doc1_2);

        //
        // Then
        assertTrue("Same Id, Different Name", equals);

        //
        // When
        equals = doc1_1.equals(doc2_1);

        //
        // Then
        assertFalse("Different Id, Same Name", equals);

        //
        // When
        equals = doc1_1.equals(doc2_2);

        //
        // Then
        assertFalse("Different Id, Different Name", equals);
    }

    private void prepareDocument() {
        DocumentIndexCreator documentIndexCreator;

        documentIndexCreator = new DocumentIndexCreator(document);
        documentIndexCreator.index(this::tokenSupplier, new StringDedup());
    }

    private Stream<String> tokenSupplier(final Document document) {
        return new SimpleTokenSupplier().stream(Utils.resource(DocumentTest.class, "-input.txt"));
    }
}
