/*
 * File header.
 */

package com.acme.phrasesearch;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.nio.charset.StandardCharsets;

import java.util.stream.Stream;

public class Utils {
    public Utils() {
        // No-op
    }

    public static Stream<String> resource(final Class<?> resourceClass, final String resourceName) {
        InputStream is = resourceClass.getResourceAsStream(resourceClass.getSimpleName() + resourceName);
        InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);

        return new BufferedReader(isr).lines();
    }
}
