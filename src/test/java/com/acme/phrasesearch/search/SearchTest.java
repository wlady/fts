/*
 * File header.
 */

package com.acme.phrasesearch.search;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.stream.Stream;

import com.acme.phrasesearch.index.Document;
import com.acme.phrasesearch.index.Index;
import com.acme.phrasesearch.index.IndexBuilder;
import com.acme.phrasesearch.index.Token;
import com.acme.phrasesearch.index.SimpleTokenSupplier;

import com.acme.phrasesearch.Utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SearchTest {
    private Index index;

    private Search search;

    public SearchTest() {
        // No-op
    }

    @Before
    public void setUp() {
        Map<Document, Boolean> documents = new HashMap<>();

        documents.put(Document.of(Paths.get("-input-1.txt")), false);
        documents.put(Document.of(Paths.get("-input-2.txt")), false);

        index = new Index();

        IndexBuilder indexBuilder;

        indexBuilder = new IndexBuilder(index, this::tokenSupplier, this::write);
        indexBuilder.run(Stream.of(documents.keySet().stream()
                                            .map(Document::getName)
                                            .toArray(Path[]::new)));

        search = new Search();
    }

    @Test
    public void testSearch_with_one_token() {
        //
        // Given
        List<String> tokens = new ArrayList<>();

        tokens.add("sed");

        //
        // When
        List<SearchResult> searchResults = search.search(index, tokens);

        //
        // Then
        assertEquals("searchResults.size", 3, searchResults.size());

        verify(searchResults, "sed", "vehicula");
        verify(searchResults, "sed", "semper");
        verify(searchResults, "sed", "vitae");
    }

    @Test
    public void testSearch_with_multiple_tokens() {
        //
        // Given
        List<String> tokens = new ArrayList<>();

        tokens.add("lorem");
        tokens.add("egestas");

        //
        // When
        List<SearchResult> searchResults = search.search(index, tokens);

        //
        // Then
        assertEquals("searchResults.size", 1, searchResults.size());

        verify(searchResults, "lorem", "egestas", "malesuada");
    }

    @Test
    public void testSearch_at_document_end() {
        //
        // Given
        List<String> tokens = new ArrayList<>();

        tokens.add("malesuada");

        //
        // When
        List<SearchResult> searchResults = search.search(index, tokens);

        //
        // Then
        assertEquals("searchResults.size", 2, searchResults.size());

        verify(searchResults, "malesuada", null);
        verify(searchResults, "malesuada", "eu");
    }

    @Test
    public void testSearch_should_return_empty_list_if_no_match() {
        //
        // Given
        List<String> tokens = new ArrayList<>();

        tokens.add("no");
        tokens.add("match");

        //
        // When
        List<SearchResult> searchResults = search.search(index, tokens);

        //
        // Then
        assertTrue("searchResults.empty", searchResults.isEmpty());
    }

    private void verify(final List<SearchResult> searchResults, String... expect) {
        for (SearchResult c : searchResults) {
            List<Token> tokens = c.getTokens();

            assertEquals(c.toString() + ".tokenCount", expect.length, tokens.size());

            boolean match = true;

            for (int i = 0; i < expect.length; i++) {
                if (!equals(expect[i], tokens.get(i))) {
                    match = false;
                    break;
                }
            }

            if (match) {
                return;
            }
        }

        fail("Not found: " + Arrays.toString(expect));
    }

    private boolean equals(final String expected, final Token actual) {
        if (expected == null && actual == null) {
            return true;
        }

        return expected != null && actual != null && expected.equals(actual.token());
    }

    private Stream<String> tokenSupplier(final Document document) {
        return new SimpleTokenSupplier().stream(Utils.resource(SearchTest.class, document.getName().toString()));
    }

    private void write(final String text) {
        // /dev/null
    }
}
