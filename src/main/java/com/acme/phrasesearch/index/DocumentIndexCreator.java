/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.time.Duration;
import java.time.Instant;

import java.util.List;
import java.util.Objects;

import java.util.stream.Collectors;

import org.apache.commons.collections4.trie.PatriciaTrie;

import com.acme.phrasesearch.utils.IntList;
import com.acme.phrasesearch.utils.StringDedup;

/**
 * Document index creator.
 *
 * <p>
 * This class is responsible for creation of document content and index.
 */
public class DocumentIndexCreator {
    private final Document document;

    /**
     * Constructor.
     *
     * @param document
     * is the document to index.
     */
    public DocumentIndexCreator(final Document document) {
        this.document = Objects.requireNonNull(document, "document");
    }

    Document getDocument() {
        return document;
    }

    /**
     * Index the document, using the given token supplier.
     *
     * @param tokenSupplier
     * is the token supplier.
     */
    void index(final TokenSupplier tokenSupplier, final StringDedup stringDedup) {
        Instant start = Instant.now();

        index0(tokenSupplier, stringDedup);

        Instant end = Instant.now();

        document.setTimeToIndex(Duration.between(start, end));
    }

    private void index0(final TokenSupplier tokenSupplier, final StringDedup stringDedup) {
        List<String> tokens = tokenSupplier.stream(document)
                                           .map(stringDedup::get)
                                           .collect(Collectors.toList());

        int size = 0;

        for (String s : tokens) {
            size += s.length();
        }

        createContentAndIndex(tokens, size + tokens.size());
    }

    private void createContentAndIndex(final List<String> tokens, final int contentSize) {
        char[] content = new char[contentSize];

        PatriciaTrie<IntList> index = new PatriciaTrie<>();

        int i = 0;

        for (String s : tokens) {
            IntList offsets;

            offsets = index.computeIfAbsent(s, k -> new IntList());
            offsets.add(i);

            char[] src = s.toCharArray();

            System.arraycopy(src, 0, content, i, src.length);

            i += src.length + 1;
        }

        document.setContent(content, index);
    }
}
