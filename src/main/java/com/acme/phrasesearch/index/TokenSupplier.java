/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.util.stream.Stream;

/**
 * Token supplier.
 */
public interface TokenSupplier {
    /**
     * Get a stream of tokens to index.
     *
     * @param document
     * is the source document.
     *
     * @return a stream of tokens.
     */
    Stream<String> stream(Document document);
}
