/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.io.IOException;

import java.nio.charset.StandardCharsets;

import java.nio.file.Files;

import java.util.List;
import java.util.ArrayList;

import java.util.stream.Stream;

/**
 * Simple token supplier.
 *
 * <p>
 * The tokens are separated by any non-letter or non-digit symbol. One letter only tokens are ignored.
 */
public class SimpleTokenSupplier implements TokenSupplier {
    /**
     * Document tokenizer.
     */
    public static class Tokenizer {
        private List<String> tokens;

        private StringBuilder tokenBuffer;

        private char[] array;

        /**
         * Constructor.
         */
        public Tokenizer() {
            // No-op
        }

        /**
         * Split the given buffer into list of tokens.
         *
         * @param buffer
         * is the buffer.
         *
         * @return a list of tokens.
         */
        public List<String> parseBuffer(final String buffer) {
            tokens = new ArrayList<>();

            tokenBuffer = new StringBuilder();

            array = buffer.toCharArray();

            for (int i = 0; i < array.length; i++) {
                char c = charAt(i);

                if (c == '\'' && tokenBuffer.length() > 0) {
                    tokenBuffer.append(c);
                    continue;
                }

                if (Character.isLetterOrDigit(c)) {
                    int ex = handleExceptions(i);

                    if (ex >= 0) {
                        i = ex - 1;
                        continue;
                    }

                    tokenBuffer.append(c);
                } else {
                    addToken();
                }
            }

            addToken();

            return tokens;
        }

        private int handleExceptions(final int i) {
            if (tokenBuffer.length() > 0) {
                return -1;
            }

            char c0 = charAt(i);

            if ((c0 == 'U' || c0 == 'u') && charAt(i + 1) == '.') {
                char c2 = charAt(i + 2);

                if ((c2 == 'S' || c2 == 's') && charAt(i + 3) == '.') {
                    int c4 = charAt(i + 4);

                    if (!Character.isLetterOrDigit(c4)) {
                        tokenBuffer.append(c0).append('.').append(c2).append('.');

                        addToken();

                        return i + 5;
                    }
                }
            }

            return -1;
        }

        private void addToken() {
            final int len = tokenBuffer.length();

            if (len > 1) {
                tokens.add(tokenBuffer.toString().toLowerCase());
            }

            tokenBuffer.delete(0, len);
        }

        private char charAt(final int i) {
            if (i < 0 || i >= array.length) {
                return '\0';
            }

            return array[i];
        }
    }

    /**
     * Constructor.
     */
    public SimpleTokenSupplier() {
        // No-op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<String> stream(final Document document) {
        try {
            return stream(Files.lines(document.getName(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Stream<String> stream(final Stream<String> buffers) {
        SimpleTokenSupplier.Tokenizer tokenizer = new SimpleTokenSupplier.Tokenizer();

        return buffers.flatMap(c -> tokenizer.parseBuffer(c).stream());
    }
}
