/*
 * Copyright (c) Experian, 2019. All rights reserved.
 */

package com.acme.phrasesearch.index;

import java.nio.file.Path;

import java.util.concurrent.atomic.AtomicInteger;

import java.time.Duration;

import org.apache.commons.collections4.trie.PatriciaTrie;

import com.acme.phrasesearch.utils.IntList;
import com.acme.phrasesearch.utils.StringDedup;

/**
 * Document.
 *
 * <p>
 * The two major components of a document are the content and the index.
 *
 * <p>
 * The content of the document is stored in normalized form. It is not the exact copy of the document, but rather a
 * list of tokens. The tokens are separated by '\0' char.
 *
 * <p>
 * The index is a map of tokens to a list of different positions in the content.
 *
 * <p>
 * Together the content and the index look like this:
 *
 * <pre>
 *  Content:
 *  +----------+ +----------+ +----------+ +----------+
 *  | Token #1 | | Token #2 | | Token #1 | | Token #3 |
 *  +----------+ +----------+ +----------+ +----------+
 *  ^            ^            ^            ^
 *  0            15           27           34
 *
 *  Index:
 *  +----------+
 *  | Token #1 | ----> Offset: [ 0, 27 ]
 *  +----------+
 *  +----------+
 *  | Token #2 | ----> Offset: [ 15 ]
 *  +----------+
 *  +----------+
 *  | Token #3 | ----> Offset: [ 34 ]
 *  +----------+
 * </pre>
 *
 */
public class Document {
    private static final AtomicInteger ID_GEN = new AtomicInteger(0);

    private final String id;

    private final Path name;

    private Duration timeToIndex;

    /**
     * Normalized document content.
     */
    private char[] content;

    /**
     * Document index.
     */
    private PatriciaTrie<IntList> index;

    /**
     * Constructor.
     *
     * @param id
     * is the document id.
     *
     * @param name
     * is the document name.
     */
    Document(final int id, final Path name) {
        this.id = String.valueOf(id);
        this.name = name;
    }

    public static Document of(final Path name) {
        return new Document(ID_GEN.incrementAndGet(), name);
    }

    /**
     * Get the token, that is after the token at the given offset.
     *
     * @param offset
     * is the offset of the current token.
     *
     * @return the next token or null, if the current token is the last one in the document.
     */
    public Token getNextToken(final int offset) {
        int nextOffset = -1;
        int nextLen = -1;

        for (int i = offset; i < content.length; i++) {
            if (content[i] == '\0') {
                nextOffset = i + 1;
                break;
            }
        }

        for (int i = nextOffset; i < content.length; i++) {
            if (content[i] == '\0') {
                nextLen = i - nextOffset;
                break;
            }
        }

        if (nextOffset < 0 || nextLen < 0) {
            return null;
        }

        return new Token(new String(content, nextOffset, nextLen), nextOffset);
    }

    /**
     * Get all offsets for the given token.
     *
     * @param token
     * is the token to search.
     *
     * @return array with the offsets of the token.
     */
    public Token[] findTokens(final String token) {
        IntList offsets = index.get(token);

        if (offsets == null) {
            return Token.EMPTY_ARRAY;
        }

        Token[] tokens = new Token[offsets.size()];

        for (int i = 0; i < tokens.length; i++) {
            tokens[i] = new Token(token, offsets.get(i));
        }

        return tokens;
    }

    /**
     * Fill the given StringDedup with the tokens from this document.
     *
     * @param stringDedup
     * is StringDedup instance.
     */
    public void dedupTokens(final StringDedup stringDedup) {
        index.keySet().forEach(stringDedup::get);
    }

    /**
     * Set the content and the index of the document.
     *
     * <p>
     * <b>Implementation Note:</b> The caller must not modify the content and the index after this method, because (for
     * better performance) the method will not make a defensive copy of the parameters.
     *
     * @param content
     * is the normalized document content.
     *
     * @param index
     * is the document index.
     */
    public void setContent(final char[] content, final PatriciaTrie<IntList> index) {
        this.content = content;

        this.index = index;
        this.index.values().forEach(IntList::compact);
    }

    public String getId() {
        return id;
    }

    public Path getName() {
        return name;
    }

    //
    // Statistics

    public int getAllTokensCount() {
        int count = 0;

        for (IntList c : index.values()) {
            count += c.size();
        }

        return count;
    }

    public int getDistinctTokensCount() {
        return index.size();
    }

    public Duration getTimeToIndex() {
        return timeToIndex;
    }

    public void setTimeToIndex(final Duration timeToIndex) {
        this.timeToIndex = timeToIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        if (content == null || index == null) {
            return "[" + id + "] " + name;
        }

        return "[" + id + "] " + name + " (content.size: " + content.length + ", index.size: " + index.size() + ")";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Document) {
            return id.equals(((Document) o).id);
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
