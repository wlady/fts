/*
 * File header.
 */

package com.acme.phrasesearch.index;

/**
 * Token.
 */
public class Token {
    public static final Token[] EMPTY_ARRAY = new Token[0];

    private final String token;

    private final int offset;

    /**
     * Constructor.
     *
     * @param token
     * is the token.
     *
     * @param offset
     * is the token offset.
     */
    public Token(final String token, final int offset) {
        this.token = token;
        this.offset = offset;
    }

    public String token() {
        return token;
    }

    public int offset() {
        return offset;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return token + ":" + offset;
    }
}
