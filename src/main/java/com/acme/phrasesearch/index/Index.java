/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.util.Set;
import java.util.Map;
import java.util.Collections;

import java.util.concurrent.ConcurrentHashMap;

/**
 * The index is a collection of documents.
 *
 * <p>
 * The class is thread-safe.
 *
 * @see Document for how the document itself is organized.
 */
public class Index {
    private final Map<Document, Object> documents;

    /**
     * Constructor.
     */
    public Index() {
        documents = new ConcurrentHashMap<>();
    }

    public Set<Document> getDocuments() {
        return Collections.unmodifiableSet(documents.keySet());
    }

    /**
     * Add the given document to the index.
     *
     * @param document
     * is the document.
     */
    void addDocument(final Document document) {
        documents.put(document, "");
    }
}
