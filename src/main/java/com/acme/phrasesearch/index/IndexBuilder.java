/*
 * File header.
 */

package com.acme.phrasesearch.index;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.Set;
import java.util.Objects;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import java.util.stream.Stream;

import com.acme.phrasesearch.InputOutput;

import com.acme.phrasesearch.utils.StringDedup;

import static java.util.stream.Collectors.toSet;

/**
 * Index builder.
 *
 * <p>
 * The run() method will index all documents in parallel.
 */
public class IndexBuilder {
    private final Index index;

    private final TokenSupplier tokenSupplier;

    private final InputOutput inOut;

    private StringDedup stringDedup;

    /**
     * Constructor.
     *
     * @param index
     * is the index.
     *
     * @param inOut
     * is the InputOutput, used to report the progress.
     */
    public IndexBuilder(final Index index, final TokenSupplier tokenSupplier, final InputOutput inOut) {
        this.index = index;
        this.tokenSupplier = tokenSupplier;

        this.inOut = inOut;
    }

    /**
     * Get a stream of paths to index.
     *
     * <p>
     * If the given name is a file, the stream will contain only that file. If the name is directory, the stream will
     * contain all files in the directory, without the files in any subdirectories.
     *
     * @param name
     * is name of a file or directory.
     *
     * @return stream of paths.
     *
     * @throws IOException if an error occurred.
     */
    public Stream<Path> documentsToIndex(final String name) throws IOException {
        Path path = Paths.get(name);

        if (Files.isDirectory(path)) {
            return Files.list(path).filter(Files::isRegularFile);
        }
        if (Files.isRegularFile(path)) {
            return Stream.of(path);
        }

        return null;
    }

    /**
     * Index documents.
     *
     * @param paths
     * are the documents to be indexed.
     */
    public void run(final Stream<Path> paths) {
        Objects.requireNonNull(paths, "paths");

        stringDedup = new StringDedup();

        collectDistinctWords();

        CompletableFuture.allOf(indexDocuments(paths)).join();
    }

    private void collectDistinctWords() {
        index.getDocuments().forEach(c -> c.dedupTokens(stringDedup));
    }

    private CompletableFuture<?>[] indexDocuments(final Stream<Path> paths) {
        Set<Path> alreadyIndexedDocuments = index.getDocuments().stream().map(Document::getName).collect(toSet());

        return paths.filter(path -> !alreadyIndexedDocuments.contains(path))
                    .map(this::asyncDocumentIndex)
                    .toArray(CompletableFuture<?>[]::new);
    }

    private CompletableFuture<?> asyncDocumentIndex(final Path path) {
        return CompletableFuture.supplyAsync(() -> indexDocument(path))
                                .handleAsync((v, e) -> completeDocument(v, e, path));
    }

    private DocumentIndexCreator indexDocument(final Path path) {
        Document document = Document.of(path);

        DocumentIndexCreator documentIndexCreator;

        documentIndexCreator = new DocumentIndexCreator(document);
        documentIndexCreator.index(tokenSupplier, stringDedup);

        return documentIndexCreator;
    }

    private DocumentIndexCreator completeDocument(
            final DocumentIndexCreator documentIndexCreator,
            final Throwable exception,
            final Path path) {

        if (exception != null) {
            Throwable ex = exception;

            if (exception instanceof CompletionException) {
                ex = exception.getCause();
            }

            if (ex instanceof RuntimeException && ex.getCause() instanceof IOException) {
                ex = ex.getCause();
            }

            inOut.write("! " + path + "\n"
                      + "  FAILED with error " + ex + ".\n");

            return null;
        }

        index.addDocument(documentIndexCreator.getDocument());

        inOut.write(". " + documentIndexCreator.getDocument().getName() + "\n");

        return documentIndexCreator;
    }
}
