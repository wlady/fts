/*
 * File header.
 */

package com.acme.phrasesearch.command;

import java.util.List;

import java.util.function.BiConsumer;

import java.util.stream.Collectors;

import com.acme.phrasesearch.InputOutput;
import com.acme.phrasesearch.MostFrequentLastToken;

import com.acme.phrasesearch.index.Index;
import com.acme.phrasesearch.index.Token;
import com.acme.phrasesearch.index.SimpleTokenSupplier;

import com.acme.phrasesearch.search.Search;
import com.acme.phrasesearch.search.SearchResult;

/**
 * Search command.
 */
public class SearchCommand implements BiConsumer<InputOutput, String> {
    private final Index index;

    /**
     * Constructor.
     *
     * @param index
     * is the index.
     */
    public SearchCommand(final Index index) {
        this.index = index;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final InputOutput inOut, final String params) {
        SimpleTokenSupplier.Tokenizer tokenSupplier = new SimpleTokenSupplier.Tokenizer();

        List<String> tokens = tokenSupplier.parseBuffer(params);

        if (tokens.isEmpty()) {
            inOut.write("Please, use more specific phrase to search for.\n");
            return;
        }

        Search search = new Search();

        List<SearchResult> results = search.search(index, tokens);

        MostFrequentLastToken analyzeResults;

        analyzeResults = new MostFrequentLastToken();
        analyzeResults.analyze(results);

        if (analyzeResults.getMostFrequent().isEmpty()) {
            inOut.write("There is no next word.\n");
            return;
        }

        inOut.write("Most frequently used next word:\n\n");
        inOut.write(DUMP_SEARCH_RESULT_HEADER);

        dumpSearchResults(inOut, analyzeResults.getMostFrequent(), 10);

        if (analyzeResults.getTop5().isEmpty()) {
            return;
        }

        inOut.write("The next top 5 most frequently used next words:\n\n");
        inOut.write(DUMP_SEARCH_RESULT_HEADER);

        dumpSearchResults(inOut, analyzeResults.getTop5(), 10);
    }

    private static final String DUMP_SEARCH_RESULT_HEADER
        = "Word                      | Frequency | Phrase\n"
        + "--------------------------+-----------+----------------------------------------------------------------------------------\n";

    private static final String DUMP_SEARCH_RESULT
        = "%-25.25s | %9d | %-80.80s";

    private static void dumpSearchResults(
            final InputOutput inOut,
            final List<MostFrequentLastToken.Item> items,
            final int limit) {

        int count = 0;

        for (MostFrequentLastToken.Item c : items) {
            if (count++ > limit) {
                inOut.write("Output is trimmed...\n");
                break;
            }

            dumpSearchResult(inOut, c);
        }

        inOut.write("\n");
    }

    private static void dumpSearchResult(final InputOutput inOut, final MostFrequentLastToken.Item item) {
        String token = item.getLastToken();
        String phrase = item.getTokens().stream().map(Token::token).collect(Collectors.joining(" "));

        inOut.write(String.format(DUMP_SEARCH_RESULT, token, item.getFrequency(), phrase) + "\n");
    }
}
