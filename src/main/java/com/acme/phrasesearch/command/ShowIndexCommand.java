/*
 * File header.
 */

package com.acme.phrasesearch.command;

import java.util.Set;
import java.util.Collection;
import java.util.Comparator;

import java.util.function.BiConsumer;

import com.acme.phrasesearch.InputOutput;

import com.acme.phrasesearch.index.Document;
import com.acme.phrasesearch.index.Index;

import static java.util.stream.Collectors.joining;

/**
 * Show index command.
 */
public class ShowIndexCommand implements BiConsumer<InputOutput, String> {
    private final Index index;

    /**
     * Constructor.
     *
     * @param index is the index.
     */
    public ShowIndexCommand(final Index index) {
        this.index = index;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final InputOutput inOut, final String params) {
        Set<Document> documents = index.getDocuments();

        if (documents.isEmpty()) {
            inOut.write("There are no indexed documents. Use /index command (for more help type /?).\n");
        }

        inOut.write(dumpDocuments(documents));
    }

    private static final String DUMP_DOC_HEADER
        = "   Id | Document                                                                         |  Tokens | Time to Index\n"
        + "------+----------------------------------------------------------------------------------+---------+--------------\n";

    private static final String DUMP_DOC
        = "%5s | %-80.80s | %7d | %s";

    private static String dumpDocuments(final Collection<Document> documents) {
        return documents.stream()
                        .sorted(Comparator.comparing(Document::getName))
                        .map(ShowIndexCommand::dumpDocument)
                        .collect(joining("\n", DUMP_DOC_HEADER, "\n"));
    }

    private static String dumpDocument(final Document doc) {
        return String.format(DUMP_DOC, doc.getId(), doc.getName(), doc.getAllTokensCount(), doc.getTimeToIndex());
    }
}
