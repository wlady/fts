/*
 * File header.
 */

package com.acme.phrasesearch.command;

import java.nio.file.Path;

import java.time.Duration;
import java.time.Instant;

import java.util.function.BiConsumer;

import java.util.stream.Stream;

import com.acme.phrasesearch.InputOutput;

import com.acme.phrasesearch.index.Index;
import com.acme.phrasesearch.index.IndexBuilder;
import com.acme.phrasesearch.index.SimpleTokenSupplier;

/**
 * Create command.
 */
public class CreateIndexCommand implements BiConsumer<InputOutput, String> {
    private final Index index;

    /**
     * Constructor.
     *
     * @param index
     * is the index.
     */
    public CreateIndexCommand(final Index index) {
        this.index = index;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final InputOutput inOut, final String params) {
        String name = params.substring(params.indexOf(' ') + 1).trim();

        IndexBuilder indexBuilder = new IndexBuilder(index, new SimpleTokenSupplier(), inOut);

        Stream<Path> paths;

        try {
            paths = indexBuilder.documentsToIndex(name);
        } catch (Exception e) {
            inOut.write("An error occurred while trying to get a list of files to index.\n"
                      + "Check that the supplied parameter is a valid file or directory and try again.\n");

            return;
        }

        if (paths == null) {
            inOut.write("The specified parameter is not a valid file or directory.\n"
                      + "Check that the file or directory exists and try again.\n");

            return;
        }

        Instant start = Instant.now();

        try {
            indexBuilder.run(paths);
        } catch (Exception e) {
            inOut.write("\n"
                      + "An error occurred while trying to index the files (" + e + ").\n"
                      + "Check that the supplied parameter is a valid file or directory and try again.\n");

            return;
        }

        Instant end = Instant.now();

        inOut.write("\n"
                  + "Indexing took " + Duration.between(start, end) + ".\n"
                  + "To see all indexed files use /index command.\n");
    }
}
