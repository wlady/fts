/*
 * File header.
 */

package com.acme.phrasesearch;

/**
 * UI Input/Output.
 *
 * <p>
 * This interface is used to abstract the usage of {@code java.io.Console}.
 */
public interface InputOutput {
    /**
     * Output text.
     *
     * @param text
     * is the text.
     */
    void write(String text);
}
