/*
 * File header.
 */

package com.acme.phrasesearch;

import com.acme.phrasesearch.command.CreateIndexCommand;
import com.acme.phrasesearch.command.SearchCommand;
import com.acme.phrasesearch.command.ShowIndexCommand;

import com.acme.phrasesearch.index.Index;

import com.acme.phrasesearch.ui.CommandLoop;

/**
 * Main app class.
 */
public class App {
    private final Index index;

    public static void main(String[] argv) {
        App app;

        app = new App();
        app.run();
    }

    /**
     * Constructor.
     */
    private App() {
        index = new Index();
    }

    private void run() {
        CommandLoop commandLoop = new CommandLoop(System.console());

        commandLoop.addCommand("showIndex", new ShowIndexCommand(index));
        commandLoop.addCommand("createIndex", new CreateIndexCommand(index));
        commandLoop.addCommand("search", new SearchCommand(index));
        commandLoop.loop();
    }
}
