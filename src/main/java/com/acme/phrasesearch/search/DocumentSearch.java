/*
 * File header.
 */

package com.acme.phrasesearch.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import com.acme.phrasesearch.index.Document;
import com.acme.phrasesearch.index.Token;

/**
 * Search a single document.
 *
 * <p>
 * The search algorithm is like this:
 *
 * <ul>
 * <li>
 * The list of offsets for the first token in the search phrase is obtained from the index.
 * </li>
 * <li>
 * For each offset a new search result is created. All search results are stored in a list.
 * </li>
 * <li>
 * Then, for each search result, the next token is added to it (the next token is taken from the content).
 * </li>
 * </ul>
 *
 * <p>
 * Then, for each remaining token in the search phrase, the following steps are executed:
 *
 * <ul>
 * <li>
 * The list with search results is filtered - the search results, whose last token is different than the current token
 * from the search phrase, are moved from the list.
 * </li>
 * <li>
 * Then, for each search result, the next token is added to it (the next token is taken from the content).
 * </li>
 * </ul>
 */
public class DocumentSearch {
    private final Document document;

    /**
     * Constructor.
     *
     * @param document
     * is the document.
     */
    public DocumentSearch(final Document document) {
        this.document = document;
    }

    /**
     * Search the document for the given phrase.
     *
     * @param tokens
     * is the phrase to search for.
     *
     * @return a list of search results. The last token in each search result will be the next token in the document
     * _after_ the given phrase. That last token could be null, if the phrase is at the end of the document and there
     * are no more tokens.
     */
    public List<SearchResult> search(final List<String> tokens) {
        Token[] tokenOffsets = document.findTokens(tokens.get(0));

        if (tokenOffsets.length == 0) {
            return Collections.emptyList();
        }

        List<SearchResult> results = createSearchResultList(tokenOffsets);

        addNextTokens(document, results);

        for (int i = 1; i < tokens.size(); i++) {
            removeNoMatches(results, tokens.get(i));
            addNextTokens(document, results);
        }

        return results;
    }

    private List<SearchResult> createSearchResultList(final Token[] offsets) {
        List<SearchResult> results = new ArrayList<>();

        for (Token c : offsets) {
            SearchResult result;

            result = new SearchResult(document);
            result.push(c);

            results.add(result);
        }

        return results;
    }

    private static void removeNoMatches(final List<SearchResult> results, final String nextToken) {
        results.removeIf(c -> noMatch(c.peek(), nextToken));
    }

    private static boolean noMatch(final Token token, final String nextToken) {
        return token == null || !token.token().equals(nextToken);
    }

    private static void addNextTokens(final Document document, final List<SearchResult> results) {
        for (SearchResult c : results) {
            c.push(document.getNextToken(c.peek().offset()));
        }
    }
}
