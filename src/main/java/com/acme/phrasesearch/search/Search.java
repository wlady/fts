/*
 * File header.
 */

package com.acme.phrasesearch.search;

import java.util.List;
import java.util.ArrayList;

import java.util.concurrent.CompletableFuture;

import com.acme.phrasesearch.index.Document;
import com.acme.phrasesearch.index.Index;

import static java.util.stream.Collectors.toList;

/**
 * Search the index.
 *
 * <p>
 * The documents in the index will be searched in parallel. The result will be combined search result from all
 * documents.
 */
public class Search {
    /**
     * Constructor.
     */
    public Search() {
        // No-op
    }

    /**
     * Search the index for the given phrase.
     *
     * @param index
     * is the index.
     *
     * @param tokens
     * is the lookup phrase.
     *
     * @return a list of search results.
     */
    public List<SearchResult> search(final Index index, final List<String> tokens) {
        List<CompletableFuture<List<SearchResult>>> documentSearch = new ArrayList<>();

        for (Document c : index.getDocuments()) {
            documentSearch.add(CompletableFuture.supplyAsync(() -> new DocumentSearch(c).search(tokens)));
        }

        return CompletableFuture.allOf(documentSearch.toArray(new CompletableFuture<?>[0]))
                                .thenApply(x -> documentSearch.stream()
                                                              .flatMap(c -> c.join().stream())
                                                              .collect(toList()))
                                .join();
    }
}
