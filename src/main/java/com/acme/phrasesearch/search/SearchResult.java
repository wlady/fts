/*
 * File header.
 */

package com.acme.phrasesearch.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import com.acme.phrasesearch.index.Document;
import com.acme.phrasesearch.index.Token;

/**
 * Search result.
 *
 * <p>
 * The search result is a list tokens. Depending on the search semantic, some tokens (in most cases the last one) in
 * the list could be null.
 */
public class SearchResult {
    private final Document document;

    private final List<Token> tokens;

    /**
     * Constructor.
     */
    public SearchResult(final Document document) {
        this.document = document;

        tokens = new ArrayList<>();
    }

    public Document getDocument() {
        return document;
    }

    public List<Token> getTokens() {
        return Collections.unmodifiableList(tokens);
    }

    /**
     * Add token.
     *
     * @param token
     * is the token to add.
     */
    public void push(final Token token) {
        tokens.add(token);
    }

    /**
     * Get the last token in the list of tokens.
     *
     * @return the last token.
     */
    public Token peek() {
        if (tokens.isEmpty()) {
            return null;
        }

        return tokens.get(tokens.size() - 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        if (tokens.isEmpty()) {
            return "#empty";
        }

        StringBuilder buff1 = new StringBuilder();
        StringBuilder buff2 = new StringBuilder();

        for (Token c : tokens) {
            if (buff1.length() > 0) {
                buff1.append(' ');
                buff2.append(':');
            }

            if (c == null) {
                buff1.append("#null");
                buff2.append("-");
            } else {
                buff1.append(c.token());
                buff2.append(c.offset());
            }
        }

        return buff1.append(" [").append(buff2).append("]").toString();
    }
}
