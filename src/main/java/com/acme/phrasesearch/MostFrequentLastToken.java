/*
 * File header.
 */

package com.acme.phrasesearch;

import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Collections;

import java.util.Comparator;

import java.util.concurrent.ConcurrentHashMap;

import com.acme.phrasesearch.index.Token;

import com.acme.phrasesearch.search.SearchResult;

import static java.util.stream.Collectors.toList;

/**
 * Sort search results by most frequently used last token.
 */
public class MostFrequentLastToken {
    public static class Item {
        private final List<Token> tokens;

        private int frequency;

        /**
         * Constructor.
         *
         * @param tokens
         * are the tokens.
         */
        public Item(final List<Token> tokens) {
            this.tokens = tokens.stream().map(c -> (c == null) ? null : new Token(c.token(), 0)).collect(toList());
        }

        public List<Token> getTokens() {
            return Collections.unmodifiableList(tokens);
        }

        public String getLastToken() {
            Token lastToken = tokens.get(tokens.size() - 1);

            if (lastToken == null) {
                return null;
            }

            return lastToken.token();
        }

        public int getFrequency() {
            return frequency;
        }

        public void increaseFrequency() {
            frequency++;
        }
    }

    private List<MostFrequentLastToken.Item> mostFrequent;
    private List<MostFrequentLastToken.Item> top5;
    private List<MostFrequentLastToken.Item> rest;
    private List<MostFrequentLastToken.Item> noLastToken;

    /**
     * Constructor.
     */
    public MostFrequentLastToken() {
        // No-op
    }

    public void analyze(final List<SearchResult> results) {
        mostFrequent = new ArrayList<>();
        top5 = new ArrayList<>();
        rest = new ArrayList<>();
        noLastToken = new ArrayList<>();

        PriorityQueue<Integer> frequencyOrder = new PriorityQueue<>(Collections.reverseOrder());

        Map<String, MostFrequentLastToken.Item> frequencyMap;

        frequencyMap = getFrequencyMap(results);
        frequencyMap.values().stream().map(Item::getFrequency).distinct().forEach(frequencyOrder::add);

        Set<Integer> mostFrequentSet;

        mostFrequentSet = new HashSet<>();
        mostFrequentSet.add(frequencyOrder.poll());

        Set<Integer> top5Set = new HashSet<>();

        for (int i = 0; i < 5; i++) {
            Integer frequency = frequencyOrder.poll();

            if (frequency != null) {
                top5Set.add(frequency);
            }
        }

        frequencyMap.values().stream()
                    .sorted(Comparator.comparing(Item::getFrequency).reversed().thenComparing(Item::getLastToken))
                    .forEach(c -> {
                        int frequency = c.getFrequency();

                        if (mostFrequentSet.contains(frequency)) {
                            mostFrequent.add(c);
                        } else if (top5Set.contains(frequency)) {
                            top5.add(c);
                        } else {
                            rest.add(c);
                        }
                    });
    }

    private Map<String, MostFrequentLastToken.Item> getFrequencyMap(final List<SearchResult> results) {
        Map<String, MostFrequentLastToken.Item> frequencyMap = new ConcurrentHashMap<>();

        results.parallelStream()
               .forEach(c -> reduceSearchResult(frequencyMap, c));

        return frequencyMap;
    }

    private void reduceSearchResult(final Map<String, MostFrequentLastToken.Item> frequencyMap, final SearchResult result) {
        final Token lastToken = result.peek();

        if (lastToken == null) {
            synchronized (noLastToken) {
                if (noLastToken.isEmpty()) {
                    noLastToken.add(new MostFrequentLastToken.Item(result.getTokens()));
                }

                noLastToken.get(0).increaseFrequency();
            }

            return;
        }

        MostFrequentLastToken.Item item;

        item = frequencyMap.computeIfAbsent(lastToken.token(), key -> new MostFrequentLastToken.Item(result.getTokens()));
        item.increaseFrequency();
    }

    public List<MostFrequentLastToken.Item> getMostFrequent() {
        return mostFrequent;
    }

    public List<MostFrequentLastToken.Item> getTop5() {
        return top5;
    }

    public List<MostFrequentLastToken.Item> getRest() {
        return rest;
    }

    public List<MostFrequentLastToken.Item> getNoLastToken() {
        return noLastToken;
    }
}
