/*
 * File header.
 */

package com.acme.phrasesearch.utils;

import java.util.Arrays;

/**
 * Array-backed list of int primitives.
 */
public class IntList {
    //
    // The only reason for this very crude implementation
    // is to avoid introduction of 3rd party dependencies.

    private static final int INCREMENT1 = 128;
    private static final int INCREMENT2 = 1024;

    private int count;
    private int[] array;

    /**
     * Constructor.
     */
    public IntList() {
        array = new int[16];
    }

    public void add(final int i) {
        if (count == array.length) {
            array = Arrays.copyOf(array, count + ((count < INCREMENT2) ? INCREMENT1 : INCREMENT2));
        }

        array[count++] = i;
    }

    public int get(final int i) {
        return array[i];
    }

    public int size() {
        return count;
    }

    /**
     * Shrink the backing array.
     */
    public void compact() {
        array = Arrays.copyOf(array, count);
    }
}
