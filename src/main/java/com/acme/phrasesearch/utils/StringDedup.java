/*
 * File header.
 */

package com.acme.phrasesearch.utils;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides interned Strings.
 *
 * <p>
 * This class is used to de-duplicate strings, so that there won't be different references for equal strings.
 *
 * <p>
 * The class is backed by ConcurrentMap, so it is thread-safe.
 */
public class StringDedup {
    private final ConcurrentMap<String, String> internedStrings;

    /**
     * Constructor.
     */
    public StringDedup() {
        internedStrings = new ConcurrentHashMap<>();
    }

    /**
     * De-duplicate the string.
     *
     * @param string
     * is the string.
     *
     * @return a string.
     */
    public String get(final String string) {
        return internedStrings.computeIfAbsent(string, key -> key);
    }
}
