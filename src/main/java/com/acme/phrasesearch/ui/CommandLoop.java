/*
 * File header.
 */

package com.acme.phrasesearch.ui;

import java.io.Console;

import java.util.Map;
import java.util.HashMap;
import java.util.Objects;

import java.util.function.BiConsumer;

import com.acme.phrasesearch.InputOutput;

/**
 * Command loop.
 */
public class CommandLoop implements InputOutput {
    private static final String USAGE =
        "Usage:\n"
      + "  <phrase>                          Phrase to search.\n"
      + "  /index                            Show currently indexed files.\n"
      + "  /index [ <file> | directory> ]    Index the file (if the argument is a file)\n"
      + "                                    or the files in the directory (excluding sub-directories).\n"
      + "  /exit                             Exit the tool.\n"
      + "  /help | /?                        Get information about using the tool.\n";

    private final Console console;

    private final Map<String, BiConsumer<InputOutput, String>> commands;

    /**
     * Constructor.
     *
     * @param console
     * is the console.
     */
    public CommandLoop(final Console console) {
        this.console = Objects.requireNonNull(console, "console");

        commands = new HashMap<>();
        commands.put("help", CommandLoop::printUsage);
    }

    public void addCommand(final String commandId, final BiConsumer<InputOutput, String> command) {
        commands.put(commandId, command);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(final String text) {
        console.printf(text);
    }

    /**
     * Start the command loop.
     *
     * <p>
     * The command loop will wait for user input and will execute commands.
     */
    public void loop() {
        write("Enter phrase to search or type /exit to exit or /? for more help.\n");

        while (true) {
            String command = console.readLine("> ");

            if (command == null) {
                break;
            }

            command = command.trim();

            if (command.isEmpty()) {
                continue;
            }

            if (command.equals("/exit")) {
                break;
            }

            if (command.equals("/help") || command.equals("/?")) {
                execute("help", command);
            } else if (command.equals("/index")) {
                execute("showIndex", command);
            } else if (command.startsWith("/index ")) { // Mind the space! This means that there are parameters (the command is trim()'ed).
                execute("createIndex", command);
            } else {
                execute("search", command);
            }
        }
    }

    private void execute(final String commandId, final String params) {
        commands.get(commandId).accept(this, params);
    }

    private static void printUsage(final InputOutput inOut, final String params) {
        long totalMemory = Runtime.getRuntime().totalMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();

        inOut.write(USAGE
                  + "\n"
                  + "Memory (total): " + totalMemory + "\n"
                  + "Memory (used): " + (totalMemory - freeMemory) + "\n"
                  + "Memory (free): " + freeMemory + "\n");
    }
}
